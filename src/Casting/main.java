package Casting;

/**
 * Created by lenin on 05/06/17.
 */
public class main {
public static void main (String args[]) {

        //Una clase padre si se puede igualarle a una clase hijo.
        Empleado em1 = new Empleado("Lenin", "Montalvo", 24, 'M',
                "Gerente", "Sistemas", 12300);
        Persona p1 = em1;

        //Una clase hijo no se puede igualar con su clase padre.
        Persona p2 = new Persona("Alexandra", "Hinojosa", 21, 'F');
        //Empleado em2 = p2;


        //Conversion por referencia.

        Persona p3 = new Persona("David", "Sánchez", 23, 'T');
        Empleado em3;
          em3 = (Empleado) p3;
           p3 = (Persona) em3;
    }
}
