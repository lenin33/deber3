package Casting;

/**
 * Created by lenin on 05/06/17.
 */
public class Empleado extends Persona{

    String cargo;
    String area;
    double sueldo;

    public Empleado(String nombre, String apellido, int edad, char sexo) {
        super(nombre, apellido, edad, sexo);
    }

    public Empleado(String nombre, String apellido, int edad, char sexo, String cargo, String area, double sueldo) {
        super(nombre, apellido, edad, sexo);
        this.cargo = cargo;
        this.area = area;
        this.sueldo = sueldo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
}
